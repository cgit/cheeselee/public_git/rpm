Name:           bumblebee
Summary:        Support for NVidia Optimus laptops on Linux!
Version:        3.0
Release:        4%{?dist}
Source0:        https://github.com/downloads/Bumblebee-Project/Bumblebee/%{name}-%{version}.tar.gz
URL:            http://bumblebee-project.org/

Group:          System Environment/Daemons
License:        GPLv3+
BuildRequires:  systemd-units
BuildRequires:  pkgconfig
BuildRequires:  glib2-devel
BuildRequires:  libX11-devel
BuildRequires:  libbsd-devel >= 0.2.0
BuildRequires:  help2man
BuildRequires:  gettext
Requires(pre):  shadow-utils
Requires(post): systemd-units
Requires(preun): systemd-units
Requires(postun): systemd-units
Requires:       VirtualGL
Requires:       bbswitch-kmod >= 0.4.1
Provides:       bbswitch-kmod-common = 0.4.1


%description
Bumblebee daemon is a rewrite of the original Bumblebee service, providing an
elegant and stable means of managing Optimus hybrid graphics chipsets.  A
primary goal of this project is to not only enable use of the discrete GPU for
rendering, but also to enable smart power management of the dGPU when it's not
in use.

%prep
%setup -q

%build
%configure --docdir=%{_docdir}/%{name}-%{version}/
make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot}
mkdir -p %{buildroot}%{_unitdir}
cp scripts/systemd/%{name}d.service %{buildroot}%{_unitdir}

%files
%{_docdir}/%{name}-%{version}/
%{_sysconfdir}/bash_completion.d/bumblebee
%{_sysconfdir}/bumblebee/bumblebee.conf
%{_sysconfdir}/bumblebee/xorg.conf.nouveau
%{_sysconfdir}/bumblebee/xorg.conf.nvidia
%{_unitdir}/bumblebeed.service
%{_sbindir}/bumblebeed
%{_bindir}/optirun
%{_bindir}/bumblebee-bugreport
%{_mandir}/man1/bumblebeed.1*
%{_mandir}/man1/optirun.1*

%pre
# Add bumblebee group
getent group %{name} >/dev/null || groupadd -r %{name}
exit 0

%post
if [ $1 -eq 1 ] ; then
    # Initial installation
    /bin/systemctl daemon-reload >/dev/null 2>&1 || :
fi

%preun
if [ $1 -eq 0 ] ; then
    # Package removal, not upgrade
    /bin/systemctl --no-reload disable %{name}d.service > /dev/null 2>&1 || :
    /bin/systemctl stop %{name}d.service > /dev/null 2>&1 || :
fi

%postun
/bin/systemctl daemon-reload >/dev/null 2>&1 || :
if [ $1 -ge 1 ] ; then
    # Package upgrade, not uninstall
    /bin/systemctl try-restart %{name}d.service >/dev/null 2>&1 || :
fi


%changelog
* Thu Feb 16 2012 Robin Lee <cheeselee@fedoraproject.org> - 3.0-4
- Deflated for Fedora

* Thu Feb 09 2012 Александр Казанцев <kazancas@mandriva.org> 3.0-3
+ Revision: 772318
- add gettext to requires and fix optirun call with all CL args
- fix spec for correct adding group and move script to %%pre section and delete group with %%postin section
- move optirun to optirun-bin
- add optirun script wich check adding current user to bumblebe group on start and say about this user.
  If group found, then optirun script running optirun-bin with $1 data.
- add localization mechanism to script.
- Updated to the 3.0 release

* Wed Jan 18 2012 Александр Казанцев <kazancas@mandriva.org> 2.99-3
+ Revision: 762167
- fix group add. Not readd group bumblebee if present allready
- Now auto-creates bumblebee group after install

* Tue Jan 17 2012 Александр Казанцев <kazancas@mandriva.org> 2.99-2
+ Revision: 762044
- add libbsd-devel as BR
- add glib-2.0-devel to BR
- imported package bumblebee


* Mon Jan 16 2012 Jaron Viëtor <thulinma@thulinma.com> 2.99-2mdk
- Improved systemd service file, daemon output now goes to dmesg
- Added dkms-bbswitch requirement

* Sun Jan 15 2012 Jaron Viëtor <thulinma@thulinma.com> 2.99-1mdk
- Initial package
