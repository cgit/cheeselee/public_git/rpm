Name:		gmchess
Version:	0.29.6
Release:	1%{?dist}
Summary:	Chinese Chess with GTKmm and C++

Group:		Amusements/Games
License:	GPLv2
URL:		http://code.google.com/p/gmchess/
Source0:	http://gmchess.googlecode.com/files/%{name}-%{version}.tar.bz2
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildRequires:	gtkmm24-devel >= 2.6.0 intltool

%description
%{summary}

%prep
%setup -q


%build
%configure
make %{?_smp_mflags}


%install
make install DESTDIR=$RPM_BUILD_ROOT
%find_lang %{name}

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING README
%{_bindir}/*
%{_libdir}/libeval.so.*
%exclude %{_libdir}/*.so
%exclude %{_libdir}/*.a
%exclude %{_libdir}/*.la
%{_datadir}/applications/*
%{_datadir}/%{name}
%{_datadir}/man/*/*
%{_datadir}/pixmaps/*



%changelog
* Thu Feb 16 2012 Robin Lee <cheeselee@fedoraproject.org> - 0.29.6-1
- Update to 0.29.6

* Wed Nov 16 2011 Robin Lee <cheeselee@fedoraproject.org> - 0.29.4-1
- Update to 0.29.4

* Mon Jun 14 2010 Robin Lee <robinlee.sysu@gmail.com> - 0.20.6-1
- renewed

* Thu May 6 2010 Kunshan Wang <wks1986@gmail.com> 0.20.6-0.wks.1
- Initial RPM release
