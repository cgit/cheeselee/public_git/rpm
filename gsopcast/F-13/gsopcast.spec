Name:           gsopcast
Version:        0.4.0
Release:        1%{?dist}
Summary:        A GUI for SopCast P2P streaming media

Group:          Applications/Multimedia
License:        GPLv2+
URL:            http://code.google.com/p/gsopcast/
Source0:        http://gsopcast.googlecode.com/files/%{name}-%{version}.tar.bz2
Patch0:         gsopcast-build.patch
Patch1:         gsopcast-desktop.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gtk2-devel
BuildRequires:  alsa-lib-devel
BuildRequires:  intltool
Requires:       /usr/bin/sp-sc

%description
GSopCast is a GUI for SopCast P2P streaming media.

%prep
%setup -q
%patch0 -p1 -b .fix
%patch1 -p1 -b .fix

%build
%configure
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

%find_lang %{name}


%clean
rm -rf $RPM_BUILD_ROOT


%files -f %{name}.lang
%defattr(-,root,root,-)
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/pixmaps/%{name}.png
%doc AUTHORS ChangeLog COPYING README



%changelog
* Sun Mar 21 2010 Cheese Lee <cheeselee@126.com> - 0.4.0-1
- Initial packaging by Cheese Lee
