Name:           linux-fetion
Version:        1.3
Release:        3%{?dist}
Summary:        Fetion for linux

Group:          Applications/Internet
License:        GPLv2
URL:            http://code.google.com/p/libfetion-gui/
Source0:        http://libfetion-gui.googlecode.com/files/linux_fetion_v%{version}.tar.gz
# fix the desktop entry
Patch0:         linux-fetion-1.3-desktop.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  qt4-devel >= 4.5.0
BuildRequires:  libcurl-devel openssl-devel libstdc++-devel
BuildRequires:  desktop-file-utils
#Requires: curl

%description
Fetion GUI Applictions of China Mobile's Fetion IM,
which is based on libfetion library.

%prep
%setup -q -n linux_fetion_v%{version}
%patch0 -p0 -b .fix
%ifarch x86_64
./64_libfetion.sh
%endif

%build
%_qt4_qmake LIBS+=" -ldl"
make  %{?_smp_mflags}


%install
rm -rf %{buildroot}

install -Dm 0644 ./misc/fetion.png %{buildroot}%{_datadir}/pixmaps/linux-fetion.png

mkdir -p %{buildroot}%{_datadir}/libfetion/
cp -rf resource skins %{buildroot}%{_datadir}/libfetion/
find %{buildroot}%{_datadir}/libfetion/ -type f -execdir chmod 0644 '{}' +

desktop-file-install --dir=%{buildroot}%{_datadir}/applications \
    ./misc/LibFetion.desktop

install -Dm 0755 linux-fetion %{buildroot}%{_bindir}/linux-fetion

%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc README COPYING ChangeLog CREDITS.txt
%{_bindir}/linux-fetion
%{_datadir}/pixmaps/linux-fetion.png
%{_datadir}/libfetion/
%{_datadir}/applications/LibFetion.desktop

%changelog
* Sun Jun 13 2010 Robin Lee <robinlee.sysu@gmail.com> - 1.3-3
- change icon name
- fix F-13 DSO linking problem

* Mon Mar  8 2010 Cheese Lee <cheeselee@126.com> - 1.3-2
- Many fixed

* Thu Jan 14 2010 Cheese Lee <cheeselee@126.com> - 1.3-1
- Update to Offical version 1.3

* Sun Nov 29 2009 Tommy He <lovenemesis@163.com> -1.2
- Update to Offical version 1.2

* Fri Oct 2 2009 Tommy He <lovenemesis@163.com> -1.1
- Update to Offical version 1.1

* Fri Jul 3 2009 Gcell <ph.linfan@gmail.com> - 1.0-1
- Update to offical version 1.0

* Mon Jun 15 2009 Gcell <ph.linfan@gmail.com> - 1.0_a2-2
- Fix files missing in source and update the spec file

* Mon Jun 15 2009 Gcell <ph.linfan@gmail.com> - 1.0_a2-1
- Update to alpha version 1.0_a2

* Sat Feb 21 2009 Gcell <ph.linfan@gmail.com> - 0.9.2-2
- update to 0.9.2 official version
- Fixed some mistakes in the spec

* Tue Feb 17 2009 Gcell <ph.linfan@gmail.com> - 0.9.2-1
- update to version 0.9.2

* Wed Dec 17 2008 Gcell <ph.linfan@gmail.com> - 0.9.1-1
- update x86_64 prepare option

* Sun Dec 14 2008 Gcell <ph.linfan@gmail.com> - 0.9.1-1
- Initial libfetion package
