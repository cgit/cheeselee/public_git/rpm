Name:       openfetion
Version:    1.6.1
Release:    2%{?dist}
Summary:    A Fetion client written using GTK+ 2

Group:      Applications/Internet
License:    GPLv2+
URL:        http://basiccoder.com/openfetion
Source0:    http://sourceforge.net/projects/ofetion/files/%{name}-%{version}.tar.gz
Patch0:     openfetion-1.6.1-desktop.patch
BuildRoot:  %{_tmppath}/%{name}-%{version}-%{release}-root

BuildRequires:  libxml2-devel, openssl-devel, gtk2-devel, libnotify-devel
BuildRequires:  gstreamer-devel
# to make an acceptable icon
BuildRequires:  ImageMagick

%package devel
Summary: Development files for Openfetion
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: libxml2-devel, openssl-devel, gtk2-devel, libnotify-devel
Requires: gstreamer-devel

%description
Openfetion is a Fetion client written using GTK+ 2, based on Fetion v4 protocol.

%description devel
This package contains header files needed for developing software which uses
Openfetion.

%prep
%setup -q
%patch0 -p0


%build
%configure
make %{?_smp_mflags}
convert skin/fetion.jpg fetion.png


%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
install -Dpm 644 fetion.png $RPM_BUILD_ROOT%{_datadir}/pixmaps/fetion.png


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc AUTHORS ChangeLog COPYING LICENSE README
%{_bindir}/*
%{_datadir}/%{name}/
%{_datadir}/applications/%{name}.desktop
%{_datadir}/pixmaps/fetion.png

%files devel
%defattr(-,root,root,-)
%{_includedir}/*

%changelog
* Sun Jun 13 2010 Robin Lee <robinlee.sysu@gmail.com> - 1.6.1-2
- Spec file massively renewed with reference to the one from Kunshan Wang
  <wks1986@gmail.com>

* Sat Jun 12 2010 Xuqing Kuang <xuqingkuang@gmail.com> - 1.6.1-1
- Initial build.

