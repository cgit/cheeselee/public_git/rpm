Name:           qipmsg
Summary:        IP Messenger clone written in Qt4
License:        GPLv2
Group:          Applications/Communications
URL:            http://code.google.com/p/qipmsg/
Version:        1.0.0
Release:        1%{?dist}
Source0:        http://qipmsg.googlecode.com/files/%{name}-%{version}.tar.bz2

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  qt4-devel
BuildRequires:  alsa-lib-devel
Requires:       xdg-utils

%description
This package contains an IP Messenger clone developed with the Qt4 toolkit.

IP Messenger is a pop up style message communication software for multiple
platforms. It is based on TCP/IP(UDP).

%prep
%setup -q

# use %{?_smp_mflags}
sed -i '/cd src && $(QMAKE) $(QMAKE_OPTS) && $(DEFS) make/s!$! %{?_smp_mflags}!' Makefile

%build
make PREFIX=%{_prefix} DOC_PATH=%{_docdir}/%{name}-%{version}/

%install
rm -rf %{buildroot}
make PREFIX=%{_prefix} DESTDIR=%{buildroot} \
     DOC_PATH=%{_docdir}/%{name}-%{version}/ install

%clean
rm -rf %{buildroot}

%post
touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
    gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%files
%defattr (-,root,root,-)
%{_docdir}/%{name}-%{version}/
%{_bindir}/qipmsg
%{_bindir}/qipmsg-xdg-open
%{_datadir}/applications/qipmsg.desktop
%{_datadir}/pixmaps/qipmsg.png
%{_datadir}/icons/hicolor/*/apps/qipmsg.png
%{_datadir}/qipmsg/

%changelog
* Sun Jun 13 2010 Robin Lee <robinlee.sysu@gmail.com> - 1.0.0-1
- update to 1.0.0

* Fri May  7 2010 Robin Lee <robinlee.sysu@gmail.com> - 0.9.9-1
- update to 0.9.9

* Mon May  3 2010 Robin Lee <robinlee.sysu@gmail.com> - 0.9.7-1
- update to 0.9.7

* Wed Feb 17 2010 Robin Lee <robinlee.sysu@gmail.com> - 0.9.5-1
- Initial packaging for Fedora based on the spec file from the upstream
