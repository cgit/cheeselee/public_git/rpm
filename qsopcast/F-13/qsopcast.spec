#%define realname qsopcast
Name:		qsopcast
Group:		Applications/Multimedia
#Group(zh_CN.UTF-8):   应用程序/互联网
Version:	0.4.86
Release:	1%{?dist}
License:	GPLv3+
Summary:	A GUI front-end for multiple P2P streaming media
Summary(zh_CN.UTF-8): P2P 流媒体程序
URL:		http://code.google.com/p/qsopcast/
Source0:	%{name}-%{version}.tar.gz
Patch0:     qsopcast-desktop.patch

BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	qt4-devel cmake
%{?_kde4_macros_api:Requires: kde4-macros(api) = %{_kde4_macros_api} }
#Requires:	sopcast
#Requires:   xpplive
#Requires:   libpps
#Requires:   xpps

%description
QSopCast is a GUI front-end for multiple P2P streaming media

%description -l zh_CN.UTF-8
P2P 的流媒体直播系统。

%prep
%setup -q
%patch0 -p0 -b .fix

%build
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=%{_prefix} -DCMAKE_BUILD_TYPE=release ..

make %{?smp_flags}

%install
rm -rf $RPM_BUILD_ROOT
cd build
make DESTDIR=$RPM_BUILD_ROOT install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr (-,root,root)
%{_bindir}/*
%{_datadir}/pixmaps/*
%{_datadir}/applications/*
%{_datadir}/apps/qsopcast/

%changelog
* Tue Mar 16 2010 Cheese Lee <cheeselee@126.com> - 0.4.86-1
- Update to 0.4.86
- Patched desktop file
- Changed group

* Tue Feb  2 2010 Cheese Lee <cheese@cheese-laptop> - 0.4.85-1
- Update to 0.4.85

* Thu May 07 2009 Liu Di <liudidi@gmail.com> - 0.3.6-1
- 与 sopcast 分开打包
- 升级到 0.3.6

* Tue Nov 28 2006 Liu Di <liudidi@gmail.com> - 0.2.4-4mgc
- update sp-sc to 1.0.1

* Mon Oct 23 2006 Liu Di <liudidi@gmail.com> - 0.2.4-1mgc
- initial RPM
