Name:           sp-auth
Version:        3.2.6
Release:        1%{?dist}
Summary:        A P2P streaming media program

Group:          Applications/Multimedia
License:        Commercial
URL:            http://www.sopcast.cn/
Source0:        http://download.sopcast.cn/download/sp-auth.tgz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
AutoReq:        0

#Requires:       /usr/lib/libstdc++.so.5
Requires:       libstdc++.so.5
#ExclusiveArch:  %ix86


%description
A P2P streaming media program

%prep
%setup -q -nsp-auth


%build


%install
rm -rf $RPM_BUILD_ROOT
%__install -D -m 0755 sp-sc-auth $RPM_BUILD_ROOT%{_bindir}/sp-sc-auth
%__ln_s sp-sc-auth $RPM_BUILD_ROOT%{_bindir}/sp-sc

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc Readme
%{_bindir}/sp-sc-auth
%{_bindir}/sp-sc



%changelog
* Tue Feb  2 2010 Cheese Lee <cheese@cheese-laptop> - 3.2.6-1
- An initial spec file from Cheese Lee
